#ifndef __TWO_D_SCENE_H__
#define __TWO_D_SCENE_H__

#include <Eigen/Core>
#include <Eigen/StdVector>

#include "Force.h"

class TwoDScene
{
public:
    
    TwoDScene();
    
    TwoDScene( int num_particles );
    
    TwoDScene( const TwoDScene& otherscene );
    
    ~TwoDScene();
    
    int getNumParticles() const;
    int getNumEdges() const;
    
    const VectorXs& getX() const;
    
    VectorXs& getX();
    
    const VectorXs& getV() const;
    
    VectorXs& getV();
    
    const VectorXs& getM() const;
    
    VectorXs& getM();
	
	const VectorXs& getVol() const;
	
	VectorXs& getVol();
	
	const VectorXs& getRadius() const;
	
	VectorXs& getRadius();
	
	const MatrixXs& getFe() const;
	
	MatrixXs& getFe();
	
    void resizeSystem( int num_particles );
    
    void setPosition( int particle, const Vector3s& pos );
    
    void setVelocity( int particle, const Vector3s& vel );
    
    void setVolume(int particle, const scalar& volume);
	
	void setFe(int particle, const Matrix3s& Fe);
    
    void setMass( int particle, const scalar& mass );
	
    void setRadius( int particle, const scalar& radius );
	
    void setFixed( int particle, bool fixed );
    
    bool isFixed( int particle ) const;
    
    void appendPosition(const VectorXs& pos);

    void clearEdges();
    
    void insertEdge( const std::pair<int,int>& edge, scalar radius );
    
    const std::vector<std::pair<int,int> >& getEdges() const;
    
    // Each halfplane is a pair of two 2D vectors, a position and a normal.
    // The the Theme 02 Milestone 01 PDF for a description of these vectors.
    
    const std::pair<int,int>& getEdge(int edg) const;
    
    void insertForce( Force* newforce );
    
    void accumulateGradU( VectorXs& F, const VectorXs& dx = VectorXs(), const VectorXs& dv = VectorXs() );
    
    void accumulateddUdxdx( MatrixXs& A, const VectorXs& dx = VectorXs(), const VectorXs& dv = VectorXs() );
    
    // Kind of a misnomer.
    void accumulateddUdxdv( MatrixXs& A, const VectorXs& dx = VectorXs(), const VectorXs& dv = VectorXs() );
    
    scalar computeKineticEnergy() const;
    scalar computePotentialEnergy() const;
    scalar computeTotalEnergy() const;
    
    void copyState( const TwoDScene& otherscene );
    
    void checkConsistency();
    
    std::vector<std::string>& getParticleTags();
    const std::vector<std::string>& getParticleTags() const;
    
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    
private:
    VectorXs m_x; //particle pos
    VectorXs m_v; //particle velocity
    VectorXs m_m; //particle mass
	VectorXs m_radius; // particle radius
    VectorXs m_vol; //particle volume
    MatrixXs m_Fe; //elastic deformation gradient
    std::vector<bool> m_fixed;
    std::vector<std::pair<int,int> > m_edges;
    
    std::vector<Eigen::Vector3i> m_faces; //store the face id
    std::vector<std::vector<int> > m_strands;

    // Forces. Note that the scene inherits responsibility for deleting forces.
    std::vector<Force*> m_forces;
    // String 'tags' assigned to particles. Can be used to identify and single out particles for special treatment.
    std::vector<std::string> m_particle_tags;
};

#endif
