//
//  testutils.cpp
//  FOSSSim
//
//  Created by chang xiao on 9/16/17.
//
//

#include "testutils.h"
using namespace std;
void testEigenConcat(){
//    cout<<"fff"<<endl;
    VectorXs v1(8);
    VectorXs v2 = VectorXs::Ones(4);
//    cout<<v2<<endl;
    v1<<v2,v2;
    v2 = v1;
    Eigen::IOFormat CommaInitFmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ", ", "", "", " << ", ";");
    v1(0)=9;
//    cout<<v1.format(CommaInitFmt)<<endl;
//    cout<<v2.format(CommaInitFmt)<<endl;
//    cout<<v1.size()<<endl;
    
    TwoDScene scene(2);
    scene.setPosition(0, Vector3s(2,2,2));
    scene.setPosition(1, Vector3s(1,1,1));
    cout<<scene.getX().format(CommaInitFmt)<<endl;
    
    VectorXs nn(5);
    nn<<4,3,54,1,4.4;
    
    scene.appendPosition(nn);
    cout<<scene.getX().format(CommaInitFmt)<<endl;
    
    
}
