#ifndef __TWO_D_SCENE_XML_PARSER_H__
#define __TWO_D_SCENE_XML_PARSER_H__

#include <Eigen/StdVector>

#include <iostream>
#include <fstream>
#include <limits>

#include "rapidxml.hpp"

#include "TwoDScene.h"

#include "ExplicitEuler.h"
#include "SemiImplicitEuler.h"
#include "ImplicitEuler.h"
#include "LinearizedImplicitEuler.h"



#include "StringUtilities.h"
#include "RenderingUtilities.h"
#include "CollisionHandler.h"


#include "TwoDSceneRenderer.h"
#include "TwoDimensionalDisplayController.h"
#include "ParticleSimulation.h"


// REALLY USEFULL TODOs
//   TODO: Improve error messages to display all valid options, etc. Could define an option class that knows its valid options and bounds on values.

// LESS USEFULL TODOs
//   TODO: Write method for computing number of a given property
//   TODO: Add some additional error checking for repeat properties, etc
//   TODO: Abstract out common code
//   TODO: Check for invalid properties

class TwoDSceneXMLParser
{
public:
    
    void loadExecutableSimulation( const std::string& file_name, bool rendering_enabled, ParticleSimulation** execsim, renderingutils::Viewport& view, scalar& dt, scalar& max_time, scalar& steps_per_sec_cap, renderingutils::Color& bgcolor, std::string& description, std::string& scenetag );
    
    // TODO: NEED AN EIGEN_ALIGNED_THING_HERE ?
private:
    
    void loadParticleSimulation( bool rendering_enabled, ParticleSimulation** execsim, renderingutils::Viewport& view, scalar& dt, renderingutils::Color& bgcolor, rapidxml::xml_node<>* node );
    
    
    
    void loadXMLFile( const std::string& filename, std::vector<char>& xmlchars, rapidxml::xml_document<>& doc );
    
    bool loadTextFileIntoString( const std::string& filename, std::string& filecontents );
    
    void loadSimulationType( rapidxml::xml_node<>* node, std::string& simtype );
    
    
    
    
//    void loadHairs(rapidxml::xml_node<>* node, TwoDScene& twodscene);

    
    void loadParticles(rapidxml::xml_node<>* node, TwoDScene& twodscene );
    
    void loadSceneTag( rapidxml::xml_node<>* node, std::string& scenetag );
    
    void loadEdges( rapidxml::xml_node<>* node, TwoDScene& twodscene );
    
    
    
    void loadIntegrator( rapidxml::xml_node<>* node, SceneStepper** scenestepper, scalar& dt );
    
    
    
    void loadMaxTime( rapidxml::xml_node<>* node, scalar& max_t );
    
    void loadMaxSimFrequency( rapidxml::xml_node<>* node, scalar& max_freq );
    
    void loadViewport( rapidxml::xml_node<> *node, renderingutils::Viewport &view);
    
    void loadBackgroundColor( rapidxml::xml_node<>* node, renderingutils::Color& color );
    
    void loadSceneDescriptionString( rapidxml::xml_node<>* node, std::string& description_string );
};

#endif
