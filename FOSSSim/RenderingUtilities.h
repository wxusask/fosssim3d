#ifndef __RENDERING_UTILITIES_H__
#define __RENDERING_UTILITIES_H__

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <list>
#include <iostream>
#include <cstdio>

#include "MathDefs.h"
#include "StringUtilities.h"

namespace renderingutils
{
	// False => error
	bool checkGLErrors();
	
	class Color
	{
	public:
		
		Color();
		
		Color( double r, double g, double b );
		
		double r;
		double g;
		double b;
	};
	
	struct Viewport
	{
	public:
		Viewport() : cx(0.0), cy(0.0), cz(0.0), size(0.) {}
		double cx;
		double cy;
		double cz;
		double rx;
		double ry;
		double rz;
		double size;
	};
	
}

#endif
