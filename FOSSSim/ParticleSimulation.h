#ifndef __PARTICLE_SIMULATION_H__
#define __PARTICLE_SIMULATION_H__

#include "TwoDScene.h"
#include "TwoDSceneRenderer.h"
#include "SceneStepper.h"
#include "CollisionHandler.h"
#include "TwoDSceneSerializer.h"
#include "ContinuousTimeUtilities.h"
#include "TwoDimensionalDisplayController.h"

// TODO: Move code out of header!
extern bool g_rendering_enabled;

class ParticleSimulation
{
public:
    
    ParticleSimulation( TwoDScene* scene, SceneStepper* scene_stepper, TwoDSceneRenderer* scene_renderer);
    
    virtual ~ParticleSimulation();
    /////////////////////////////////////////////////////////////////////////////
    // Simulation Control Functions
    
    void stepSystem( const scalar& dt );
    
    /////////////////////////////////////////////////////////////////////////////
    // Rendering Functions
    
    void initializeOpenGLRenderer();
    void renderSceneOpenGL();
    void updateOpenGLRendererState();
    void computeCameraCenter(renderingutils::Viewport& view);


    /////////////////////////////////////////////////////////////////////////////
    // Serialization Functions
    void serializeScene( std::ofstream& outputstream );
    
    /////////////////////////////////////////////////////////////////////////////
    // Status Functions
    
	std::string getSolverName();
	
	void centerCamera(bool b_reshape = true);
	void keyboard( unsigned char key, int x, int y );
	void reshape( int w, int h );
	
	void special( int key, int x, int y );
	
	void mouse( int button, int state, int x, int y );
	
	void translateView( double dx, double dy );
	
	void zoomView( double dx, double dy );
	
	void motion( int x, int y );
	
	int getWindowWidth() const;
	int getWindowHeight() const;
	
	void setWindowWidth(int w);
	void setWindowHeight(int h);
	
	TwoDimensionalDisplayController* getDC();
	
	void setCenterX( double x );
	void setCenterY( double y );
	void setCenterZ( double z );
	void setScaleFactor( double scale );
	
	void setCamera( const Camera& cam );
	void setView( const renderingutils::Viewport& view );
private:
    TwoDScene* m_scene;
    SceneStepper* m_scene_stepper;
    TwoDSceneRenderer* m_scene_renderer;
    TwoDSceneSerializer m_scene_serializer;
	TwoDimensionalDisplayController m_display_controller;
    
};

#endif
