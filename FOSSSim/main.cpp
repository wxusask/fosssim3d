#include <Eigen/StdVector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <time.h>

#include <tclap/CmdLine.h>

// TODO: Clean these up, we don't need them all anymore.
#include "TwoDScene.h"
#include "Force.h"
#include "ExplicitEuler.h"
#include "SemiImplicitEuler.h"
#include "TwoDimensionalDisplayController.h"
#include "TwoDSceneRenderer.h"
#include "TwoDSceneXMLParser.h"
#include "TwoDSceneSerializer.h"
#include "StringUtilities.h"
#include "MathDefs.h"
#include "TimingUtilities.h"
#include "RenderingUtilities.h"
#include "YImage.h"
#include "CollisionHandler.h"

#include "testutils.h"
#include "ParticleSimulation.h"

///////////////////////////////////////////////////////////////////////////////
// Contains the actual simulation, renderer, parser, and serializer
ParticleSimulation* g_executable_simulation;


///////////////////////////////////////////////////////////////////////////////
// Rendering State
bool g_rendering_enabled = true;
double g_sec_per_frame;
double g_last_time = timingutils::seconds();

renderingutils::Color g_bgcolor(1.0,1.0,1.0);

///////////////////////////////////////////////////////////////////////////////
// SVG Rendering State



///////////////////////////////////////////////////////////////////////////////
// Parser state
std::string g_xml_scene_file;
std::string g_description;
std::string g_scene_tag = "";


///////////////////////////////////////////////////////////////////////////////
// Scene input/output/comparison state
bool g_save_to_binary = false;
std::string g_binary_file_name;
std::ofstream g_binary_output;


///////////////////////////////////////////////////////////////////////////////
// Simulation state
bool g_paused = true;
scalar g_dt = 0.0;
int g_num_steps = 0;
int g_current_step = 0;
bool g_simulation_ran_to_completion = false;



///////////////////////////////////////////////////////////////////////////////
// Simulation functions

void miscOutputCallback();
void sceneScriptingCallback();
void dumpPNG(const std::string &filename);

void stepSystem()
{
	assert( !(g_save_to_binary) );
	
	// Determine if the simulation is complete
	if( g_current_step >= g_num_steps )
	{
		std::cout << outputmod::startpink << "FOSSSim message: " << outputmod::endpink << "Simulation complete at time " << g_current_step*g_dt << ". Exiting." << std::endl;
		g_simulation_ran_to_completion = true;
		exit(0);
	}
	
	// If comparing simulations, copy state of comparison scene to simulated scene
 
	
	// Step the system forward in time
	g_executable_simulation->stepSystem(g_dt);
	g_current_step++;
	
	// If the user wants to save output to a binary
	if( g_save_to_binary )
	{
		g_executable_simulation->serializeScene(g_binary_output);
	}
	
	
	// Update the state of the renderers
	if( g_rendering_enabled ) g_executable_simulation->updateOpenGLRendererState();
	
	// If comparing simulations, load comparison scene's equivalent step
	
	
	// If the user wants to generate a SVG movie

	
	
	
	// If the user wants to generate a PNG movie
#ifdef PNGOUT
	std::stringstream oss;
	oss << "pngs/frame" << std::setw(5) << std::setfill('0') << g_current_step << ".png";
	dumpPNG(oss.str());
#endif
	
	// Execute the user-customized output callback
	miscOutputCallback();
	
	// Execute the user-customized scripting callback
	sceneScriptingCallback();
}

void syncScene()
{
	if( g_save_to_binary )
	{
		g_executable_simulation->serializeScene(g_binary_output);
	}
	
	
}

void headlessSimLoop()
{
	scalar nextpercent = 0.02;
	std::cout << outputmod::startpink << "Progress: " << outputmod::endpink;
	for( int i = 0; i < 50; ++i ) std::cout << "-";
	std::cout << std::endl;
	std::cout << "          ";
	while( true )
	{
		scalar percent_done = ((double)g_current_step)/((double)g_num_steps);
		if( percent_done >= nextpercent )
		{
			nextpercent += 0.02;
			std::cout << "." << std::flush;
		}
		stepSystem();
	}
}



///////////////////////////////////////////////////////////////////////////////
// Rendering and UI functions

void dumpPNG(const std::string &filename)
{
#ifdef PNGOUT
	YImage image;
	image.resize(g_display_controller.getWindowWidth(), g_display_controller.getWindowHeight());
	
	glPixelStorei(GL_PACK_ALIGNMENT, 4);
	glPixelStorei(GL_PACK_ROW_LENGTH, 0);
	glPixelStorei(GL_PACK_SKIP_ROWS, 0);
	glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
	glReadBuffer(GL_BACK);
	
	glFinish();
	glReadPixels(0, 0, g_display_controller.getWindowWidth(), g_display_controller.getWindowHeight(), GL_RGBA, GL_UNSIGNED_BYTE, image.data());
	image.flip();
	
	image.save(filename.c_str());
#endif
}

void reshape( int w, int h )
{
	g_executable_simulation->reshape(w, h);
	
	assert( renderingutils::checkGLErrors() );
}

// TODO: Move these functions to scene renderer?
void setOrthographicProjection()
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	
	gluOrtho2D(0, g_executable_simulation->getWindowWidth(), 0, g_executable_simulation->getWindowHeight());
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	
	assert( renderingutils::checkGLErrors() );
}

void renderBitmapString( float x, float y, float z, void *font, std::string s )
{
	glRasterPos3f(x, y, z);
	for( std::string::iterator i = s.begin(); i != s.end(); ++i )
	{
		char c = *i;
		glutBitmapCharacter(font, c);
	}
	
	assert( renderingutils::checkGLErrors() );
}

void drawHUD()
{
	setOrthographicProjection();
	glColor3f(1.0-g_bgcolor.r,1.0-g_bgcolor.g,1.0-g_bgcolor.b);
	renderBitmapString( 4, g_executable_simulation->getWindowHeight()-20, 0.0, GLUT_BITMAP_HELVETICA_18, stringutils::convertToString(g_current_step*g_dt) );
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	
	assert( renderingutils::checkGLErrors() );
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	
	glMatrixMode(GL_MODELVIEW);
	
	assert( g_executable_simulation != NULL );
	g_executable_simulation->renderSceneOpenGL();
	
	drawHUD();
	
	glutSwapBuffers();
	
	assert( renderingutils::checkGLErrors() );
}

void keyboard( unsigned char key, int x, int y )
{
	g_executable_simulation->keyboard(key,x,y);
	
	if( key == 27 || key == 'q' )
	{
		exit(0);
	}
	else if( key == 's' || key =='S' )
	{
		stepSystem();
		glutPostRedisplay();
	}
	else if( key == ' ' )
	{
		g_paused = !g_paused;
	}
	else if( key == 'c' || key == 'C' )
	{
		g_executable_simulation->centerCamera();

		glutPostRedisplay();
	}
	else if( key == 'i' || key == 'I' )
	{
		std::cout << outputmod::startpink << "FOSSSim message: " << outputmod::endpink << "Saving screenshot as 'output.svg'." << std::endl;
	}
	
	assert( renderingutils::checkGLErrors() );
}

// Proccess 'special' keys
void special( int key, int x, int y )
{
	g_executable_simulation->special(key,x,y);
	
	assert( renderingutils::checkGLErrors() );
}

void mouse( int button, int state, int x, int y )
{
	g_executable_simulation->mouse(button,state,x,y);
	
	assert( renderingutils::checkGLErrors() );
}

void motion( int x, int y )
{
	g_executable_simulation->motion(x,y);
	
	assert( renderingutils::checkGLErrors() );
}

void idle()
{
	//std::cout << "g_last_time: " << g_last_time << std::endl;
	// Trigger the next timestep
	double current_time = timingutils::seconds();
	//std::cout << "current_time: " << current_time << std::endl;
	//std::cout << "g_sec_per_frame: " << g_sec_per_frame << std::endl;
	if( !g_paused && current_time-g_last_time >= g_sec_per_frame )
	{
		g_last_time = current_time;
		stepSystem();
		glutPostRedisplay();
	}
	
	assert( renderingutils::checkGLErrors() );
}

void initializeOpenGLandGLUT( int argc, char** argv )
{
	// Initialize GLUT
	glutInit(&argc,argv);
	//glutInitDisplayString("rgba stencil=8 depth=24 samples=4 hidpi");
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_MULTISAMPLE | GLUT_DEPTH);
	
	int scrwidth = glutGet(GLUT_SCREEN_WIDTH);
	int scrheight = glutGet(GLUT_SCREEN_HEIGHT);
	
	
	int factor = std::min(scrwidth / 320, scrheight / 180);
	scrwidth = factor * 320;
	scrheight = factor * 180;
	/*#ifdef __APPLE__
	 scrwidth *= 2;
	 scrheight *= 2;
	 #endif*/
	g_executable_simulation->setWindowWidth(scrwidth);
	g_executable_simulation->setWindowHeight(scrheight);

	glutInitWindowSize(g_executable_simulation->getWindowWidth(),g_executable_simulation->getWindowHeight());
	glutCreateWindow("libElasto");
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(special);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutIdleFunc(idle);
	
	glEnable(GL_MULTISAMPLE_ARB);
	glHint(GL_MULTISAMPLE_FILTER_HINT_NV, GL_NICEST);
	// Initialize OpenGL
	reshape(scrwidth,scrheight);
	glClearColor(g_bgcolor.r, g_bgcolor.g, g_bgcolor.b, 1.0);
	
	glEnable(GL_BLEND);
	glEnable(GL_POLYGON_SMOOTH);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	
	GLubyte halftone[] = {
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55,
		0xAA, 0xAA, 0xAA, 0xAA, 0x55, 0x55, 0x55, 0x55};
	
	glPolygonStipple(halftone);
	
	g_executable_simulation->initializeOpenGLRenderer();
	
	assert( renderingutils::checkGLErrors() );
}


///////////////////////////////////////////////////////////////////////////////
// Parser functions

void loadScene( const std::string& file_name )
{
	// Maximum time in the simulation to run for. This has nothing to do with run time, cpu time, etc. This is time in the 'virtual world'.
	scalar max_time;
	// Maximum frequency, in wall clock time, to execute the simulation for. This serves as a cap for simulations that run too fast to see a solution.
	scalar steps_per_sec_cap = 100.0;
	// Contains the center and 'scale factor' of the view
	renderingutils::Viewport view;
	
	// Load the simulation and pieces of rendring and UI state
	assert( g_executable_simulation == NULL );
	TwoDSceneXMLParser xml_scene_parser;
	xml_scene_parser.loadExecutableSimulation( file_name, g_rendering_enabled, &g_executable_simulation,
											  view, g_dt, max_time, steps_per_sec_cap, g_bgcolor, g_description, g_scene_tag );
	assert( g_executable_simulation != NULL );
	
	// If the user did not request a custom viewport, try to compute a reasonable default.
	g_executable_simulation->centerCamera(false);
	
	// To cap the framerate, compute the minimum time a single timestep should take
	g_sec_per_frame = 1.0/steps_per_sec_cap;
	// Integer number of timesteps to take
	g_num_steps = ceil(max_time/g_dt);
	// We begin at the 0th timestep
	g_current_step = 0;
}

void parseCommandLine( int argc, char** argv )
{
	try
	{
		TCLAP::CmdLine cmd("Forty One Sixty Seven Sim");
		
		// XML scene file to load
		TCLAP::ValueArg<std::string> scene("s", "scene", "Simulation to run; an xml scene file", true, "", "string", cmd);
		
		// Begin the scene paused or running
		TCLAP::ValueArg<bool> paused("p", "paused", "Begin the simulation paused if 1, running if 0", false, true, "boolean", cmd);
		
		// Run the simulation with rendering enabled or disabled
		TCLAP::ValueArg<bool> display("d", "display", "Run the simulation with display enabled if 1, without if 0", false, true, "boolean", cmd);
		
		// These cannot be set at the same time
		// File to save output to
		TCLAP::ValueArg<std::string> output("o", "outputfile", "Binary file to save simulation state to", false, "", "string", cmd);
		// File to load for comparisons
		TCLAP::ValueArg<std::string> input("i", "inputfile", "Binary file to load simulation state from for comparison", false, "", "string", cmd);
		
		// Save svgs to a movie directory
		TCLAP::ValueArg<std::string> movie("m", "moviedir", "Directory to output svg screenshot to", false, "", "string", cmd);
		
		cmd.parse(argc, argv);
		
		if( output.isSet() && input.isSet() ) throw TCLAP::ArgException( "arguments i and o specified simultaneously", "arguments i and o", "invalid argument combination" );
		
		assert( scene.isSet() );
		g_xml_scene_file = scene.getValue();
		g_paused = paused.getValue();
		g_rendering_enabled = display.getValue();
		
		if( output.isSet() )
		{
			g_save_to_binary = true;
			g_binary_file_name = output.getValue();
		}
		
//		if( input.isSet() )
//		{
//			g_simulate_comparison = true;
//			g_comparison_file_name = input.getValue();
//		}
//		
//		if( movie.isSet() )
//		{
//			g_svg_enabled = true;
//			g_movie_dir = movie.getValue();
//		}
	}
	catch (TCLAP::ArgException& e)
	{
		std::cerr << "error: " << e.what() << std::endl;
		exit(1);
	}
}



///////////////////////////////////////////////////////////////////////////////
// Various support functions

void miscOutputFinalization();

void cleanupAtExit()
{
	if( g_binary_output.is_open() )
	{
		std::cout << outputmod::startpink << "FOSSSim message: " << outputmod::endpink << "Saved simulation to file '" << g_binary_file_name << "'." << std::endl;
		g_binary_output.close();
	}
	
//	if( g_binary_input.is_open() )
//	{
//		std::cout << outputmod::startpink << "FOSSSim message: " << outputmod::endpink << "Colsing benchmarked simulation file '" << g_comparison_file_name << "'." << std::endl;
//	}
	
	
	miscOutputFinalization();
	
	if( g_executable_simulation != NULL )
	{
		delete g_executable_simulation;
		g_executable_simulation = NULL;
	}
}

std::ostream& fosssim_header( std::ostream& stream )
{
	stream << outputmod::startgreen <<
	"------------------------------------------    " << std::endl <<
	"  _____ ___  ____ ____ ____  _                " << std::endl <<
	" |  ___/ _ \\/ ___/ ___/ ___|(_)_ __ ___      " << std::endl <<
	" | |_ | | | \\___ \\___ \\___ \\| | '_ ` _ \\ " << std::endl <<
	" |  _|| |_| |___) |__) |__) | | | | | | |     " << std::endl <<
	" |_|   \\___/|____/____/____/|_|_| |_| |_|    " << std::endl <<
	"------------------------------------------    "
	<< outputmod::endgreen << std::endl;
	
	return stream;
}

std::ofstream g_debugoutput;

void miscOutputInitialization()
{
	//g_debugoutput.open("debugoutput.txt");
	//g_debugoutput << "# Time   PotentialEnergy   KineticEnergy   TotalEnergy" << std::endl;
	//g_debugoutput << g_current_step*g_dt << "\t" << g_scene.computePotentialEnergy() << "\t" << g_scene.computeKineticEnergy() << "\t" << g_scene.computeTotalEnergy() << std::endl;
}

void miscOutputCallback()
{
	//g_debugoutput << g_current_step*g_dt << "\t" << g_scene.computePotentialEnergy() << "\t" << g_scene.computeKineticEnergy() << "\t" << g_scene.computeTotalEnergy() << std::endl;
}

void miscOutputFinalization()
{
	//g_debugoutput.close();
}

// Called at the end of each timestep. Intended for adding effects to creative scenes.
void sceneScriptingCallback()
{
	//  // If the scene is one we wish to 'script'
	//  if( g_scene_tag == "ParticleFountain" )
	//  {
	//    // Get the particle tags
	//    const std::vector<std::string>& tags = g_scene.getParticleTags();
	//    // Get the particle positions
	//    VectorXs& x = g_scene.getX();
	//    // Get the particle velocities
	//    VectorXs& v = g_scene.getV();
	//    // Get the particle colors
	//    std::vector<renderingutils::Color>& pcolors = g_scene_renderer->getParticleColors();
	//
	//    // If any particles are tagged for teleportation and fall below -1.25
	//    for( std::vector<std::string>::size_type i = 0; i < tags.size(); ++i ) if( tags[i] == "teleport" && x(2*i+1) < -1.25 )
	//    {
	//      // Return this particle to the origin
	//      x.segment<2>(2*i).setZero();
	//      // Give this particle some random upward velocity
	//      double vx = 0.2*(((double)rand())/((double)RAND_MAX)-0.5);
	//      double vy = 0.15*((double)rand())/((double)RAND_MAX);
	//      v.segment<2>(2*i) << vx, vy;
	//      // Assign the particle a random color
	//      pcolors[i].r = ((double)rand())/((double)RAND_MAX);
	//      pcolors[i].g = ((double)rand())/((double)RAND_MAX);
	//      pcolors[i].b = ((double)rand())/((double)RAND_MAX);
	//    }
	//  }
}


const long g_hardcode = '7614';
const char* g_root = "/home/cs4167/oracle/student_info/";
const double min_rerun_sec = 900.0; // 15 mins

bool check_availability()
{
	long uid = (long) geteuid();
	long uid_xor = uid ^ g_hardcode;
	
	std::string szfn = g_root + stringutils::convertToString(uid_xor);
	std::ifstream timestamp_input;
	timestamp_input.open(szfn.c_str());
	
	bool avail = false;
	
	if(!timestamp_input.fail())
	{
		time_t ts;
		timestamp_input.read((char*) &ts, sizeof(time_t));
		timestamp_input.close();
		
		time_t tnow;
		time(&tnow);
		
		double seconds = difftime(tnow, ts);
		if(seconds < min_rerun_sec) return false;
		else avail = true;
	} else {
		avail = true;
	}
	
	std::ofstream timestamp_output;
	timestamp_output.open(szfn.c_str());
	time_t tnow;
	time(&tnow);
	timestamp_output.write((char*)&tnow, sizeof(time_t));
	timestamp_output.close();
	
	return avail;
}

int main( int argc, char** argv )
{
	// Parse command line arguments
	parseCommandLine( argc, argv );
	
	assert( !(g_save_to_binary ) );
	
	// Function to cleanup at progarm exit
	atexit(cleanupAtExit);
	
	// Load the user-specified scene
	loadScene(g_xml_scene_file);
	
	// If requested, open the binary output file
	if( g_save_to_binary )
	{
		// Attempt to open the binary
		g_binary_output.open(g_binary_file_name.c_str());
		if( g_binary_output.fail() )
		{
			std::cerr << outputmod::startred << "ERROR IN INITIALIZATION: "  << outputmod::endred << "Failed to open binary output file: " << " `" << g_binary_file_name << "`   Exiting." << std::endl;
			exit(1);
		}
		// Save the initial conditions
		g_executable_simulation->serializeScene(g_binary_output);
	}
	// If requested, open the input file for the scene to benchmark
	
	// Initialization for OpenGL and GLUT
	if( g_rendering_enabled ) initializeOpenGLandGLUT(argc,argv);
	
	// Print a header
	std::cout << fosssim_header << std::endl;
	
	// Print some status info about this FOSSSim build
#ifdef FOSSSIM_VERSION
	std::cout << outputmod::startblue << "FOSSSim Version: "  << outputmod::endblue << FOSSSIM_VERSION << std::endl;
#endif
#ifdef CMAKE_BUILD_TYPE
	std::cout << outputmod::startblue << "Build type: " << outputmod::endblue << CMAKE_BUILD_TYPE << std::endl;
#endif
#ifdef EIGEN_VECTORIZE
	std::cout << outputmod::startblue << "Vectorization: " << outputmod::endblue << "Enabled" << std::endl;
#else
	std::cout << outputmod::startblue << "Vectorization: " << outputmod::endblue << "Disabled" << std::endl;
#endif
	
	std::cout << outputmod::startblue << "Scene: " << outputmod::endblue << g_xml_scene_file << std::endl;
	std::cout << outputmod::startblue << "Integrator: " << outputmod::endblue << g_executable_simulation->getSolverName() << std::endl;
	
	std::cout << outputmod::startblue << "Description: " << outputmod::endblue << g_description << std::endl;
	
	if( g_save_to_binary ) std::cout << outputmod::startpink << "FOSSSim message: "  << outputmod::endpink << "Saving simulation to: " << g_binary_file_name << std::endl;

	
	miscOutputInitialization();
	
	if( g_rendering_enabled ) glutMainLoop();
	else headlessSimLoop();
	
	return 0;
}
