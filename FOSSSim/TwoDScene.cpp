#include "TwoDScene.h"
#include <iostream>

TwoDScene::TwoDScene()
: m_x()
, m_v()
, m_m()
, m_fixed()
, m_edges()
, m_forces()
, m_particle_tags()
{}

TwoDScene::TwoDScene( int num_particles )
: m_x(4*num_particles)
, m_v(4*num_particles)
, m_m(4*num_particles)
, m_vol(num_particles)
, m_Fe(num_particles * 3, 3)
, m_radius(num_particles)
, m_fixed(num_particles)
, m_edges()
, m_forces()
, m_particle_tags()
{
    assert( num_particles >= 0 );
}

TwoDScene::TwoDScene( const TwoDScene& otherscene )
: m_x(otherscene.m_x)
, m_v(otherscene.m_v)
, m_m(otherscene.m_m)
, m_fixed(otherscene.m_fixed)
, m_vol(otherscene.m_vol)
, m_radius(otherscene.m_radius)
, m_Fe(otherscene.m_Fe)
, m_edges()
, m_forces()
, m_particle_tags()
{
    m_forces.resize(otherscene.m_forces.size());
    for( std::vector<Force*>::size_type i = 0; i < m_forces.size(); ++i ) m_forces[i] = otherscene.m_forces[i]->createNewCopy();
}

TwoDScene::~TwoDScene()
{
    for( std::vector<Force*>::size_type i = 0; i < m_forces.size(); ++i )
    {
        assert( m_forces[i] != NULL );
        delete m_forces[i];
        m_forces[i] = NULL;
        
    }
}

int TwoDScene::getNumParticles() const
{
    return m_x.size()/4;
}

int TwoDScene::getNumEdges() const
{
    return m_edges.size();
}

const VectorXs& TwoDScene::getX() const
{
    return m_x;
}

VectorXs& TwoDScene::getX()
{
    return m_x;
}

const VectorXs& TwoDScene::getV() const
{
    return m_v;
}

VectorXs& TwoDScene::getV()
{
    return m_v;
}

const VectorXs& TwoDScene::getM() const
{
    return m_m;
}

VectorXs& TwoDScene::getM()
{
    return m_m;
}

const VectorXs& TwoDScene::getVol() const
{
	return m_vol;
}

VectorXs& TwoDScene::getVol()
{
	return m_vol;
}

const VectorXs& TwoDScene::getRadius() const
{
	return m_radius;
}

VectorXs& TwoDScene::getRadius()
{
	return m_radius;
}

const MatrixXs& TwoDScene::getFe() const
{
	return m_Fe;
}

MatrixXs& TwoDScene::getFe()
{
	return m_Fe;
}

void TwoDScene::resizeSystem( int num_particles )
{
    assert( num_particles >= 0 );
    
    m_x.resize(4*num_particles);
    m_v.resize(4*num_particles);
    m_m.resize(4*num_particles);
    m_vol.resize(num_particles);
	m_radius.resize(num_particles);
	m_Fe.resize(num_particles * 3, 3);
    m_fixed.resize(num_particles);
    m_particle_tags.resize(num_particles);
}

void TwoDScene::setPosition( int particle, const Vector3s& pos )
{
    assert( particle >= 0 );
    assert( particle < getNumParticles() );
    
    m_x.segment<3>(4*particle) = pos;
}

void TwoDScene::setVelocity( int particle, const Vector3s& vel )
{
    assert( particle >= 0 );
    assert( particle < getNumParticles() );
    
    m_v.segment<3>(4*particle) = vel;
}

void TwoDScene::setVolume(int particle, const scalar& volume){
    assert(particle >= 0);
    assert(particle < getNumParticles());
    m_vol(particle) = volume;
}

void TwoDScene::setRadius(int particle, const scalar& radius){
	assert(particle >= 0);
	assert(particle < getNumParticles());
	m_radius(particle) = radius;
}

void TwoDScene::setFe(int particle, const Matrix3s& Fe)
{
	assert(particle >= 0);
	assert(particle < getNumParticles());
	m_Fe.block<3,3>(particle, 0) = Fe;
}

void TwoDScene::setMass( int particle, const scalar& mass )
{
    assert( particle >= 0 );
    assert( particle < getNumParticles() );
    
    m_m(4*particle)   = mass;
    m_m(4*particle+1) = mass;
    m_m(4*particle+2) = mass;
    m_m(4*particle+3) = 1.0;
}

void TwoDScene::setFixed( int particle, bool fixed )
{
    assert( particle >= 0 );
    assert( particle < getNumParticles() );
    
    m_fixed[particle] = fixed;
}

bool TwoDScene::isFixed( int particle ) const
{
    assert( particle >= 0 );
    assert( particle < getNumParticles() );
    
    return m_fixed[particle];
}

void TwoDScene::appendPosition(const VectorXs& pos){
    VectorXs newVec(pos.size()+m_x.size());
    newVec << m_x, pos;
    m_x = newVec;
}

void TwoDScene::clearEdges()
{
    m_edges.clear();
}



void TwoDScene::insertEdge( const std::pair<int,int>& edge, scalar radius )
{
    assert( edge.first >= 0 );  assert( edge.first < getNumParticles() );
    assert( edge.second >= 0 ); assert( edge.second < getNumParticles() );
    assert( radius > 0.0 );
    
    m_edges.push_back(edge);
}


const std::vector<std::pair<int,int> >& TwoDScene::getEdges() const
{
    return m_edges;
}


const std::pair<int,int>& TwoDScene::getEdge(int edg) const
{
    assert( edg >= 0 );
    assert( edg < (int) m_edges.size() );
    
    return m_edges[edg];
}


void TwoDScene::insertForce( Force* newforce )
{
    m_forces.push_back(newforce);
}

scalar TwoDScene::computeKineticEnergy() const
{
    scalar T = 0.0;
    for( int i = 0; i < getNumParticles(); ++i ){
        T += m_m(4*i)*m_v.segment<3>(4*i).dot(m_v.segment<3>(4*i));
    }
    return 0.5*T;
}

scalar TwoDScene::computePotentialEnergy() const
{
    scalar U = 0.0;
    for( std::vector<Force*>::size_type i = 0; i < m_forces.size(); ++i ) m_forces[i]->addEnergyToTotal( m_x, m_v, m_m, U );
    return U;
}

scalar TwoDScene::computeTotalEnergy() const
{
    return computeKineticEnergy()+computePotentialEnergy();
}

void TwoDScene::accumulateGradU( VectorXs& F, const VectorXs& dx, const VectorXs& dv )
{
    assert( F.size() == m_x.size() );
    assert( dx.size() == dv.size() );
    assert( dx.size() == 0 || dx.size() == F.size() );
    
    // Accumulate all energy gradients
    if( dx.size() == 0 ) for( std::vector<Force*>::size_type i = 0; i < m_forces.size(); ++i ) m_forces[i]->addGradEToTotal( m_x, m_v, m_m, F );
    else                 for( std::vector<Force*>::size_type i = 0; i < m_forces.size(); ++i ) m_forces[i]->addGradEToTotal( m_x+dx, m_v+dv, m_m, F );
}

void TwoDScene::accumulateddUdxdx( MatrixXs& A, const VectorXs& dx, const VectorXs& dv )
{
    assert( A.rows() == m_x.size() );
    assert( A.cols() == m_x.size() );
    assert( dx.size() == dv.size() );
    assert( dx.size() == 0 || dx.size() == A.rows() );
    
    if( dx.size() == 0 ) for( std::vector<Force*>::size_type i = 0; i < m_forces.size(); ++i ) m_forces[i]->addHessXToTotal( m_x, m_v, m_m, A );
    else                 for( std::vector<Force*>::size_type i = 0; i < m_forces.size(); ++i ) m_forces[i]->addHessXToTotal( m_x+dx, m_v+dv, m_m, A );
}

void TwoDScene::accumulateddUdxdv( MatrixXs& A, const VectorXs& dx, const VectorXs& dv )
{
    assert( A.rows() == m_x.size() );
    assert( A.cols() == m_x.size() );
    assert( dx.size() == dv.size() );
    assert( dx.size() == 0 || dx.size() == A.rows() );
    
    if( dx.size() == 0 ) for( std::vector<Force*>::size_type i = 0; i < m_forces.size(); ++i ) m_forces[i]->addHessVToTotal( m_x, m_v, m_m, A );
    else                 for( std::vector<Force*>::size_type i = 0; i < m_forces.size(); ++i ) m_forces[i]->addHessVToTotal( m_x+dx, m_v+dv, m_m, A );
}

void TwoDScene::copyState( const TwoDScene& otherscene )
{
    m_x = otherscene.m_x;
    m_v = otherscene.m_v;
    m_m = otherscene.m_m;
    m_fixed = otherscene.m_fixed;
    m_edges = otherscene.m_edges;
    
    for( std::vector<Force*>::size_type i = 0; i < m_forces.size(); ++i )
    {
        assert( m_forces[i] != NULL );
        delete m_forces[i];
        m_forces[i] = NULL;
    }
    m_forces.resize(otherscene.m_forces.size());
    for( std::vector<Force*>::size_type i = 0; i < m_forces.size(); ++i ) m_forces[i] = otherscene.m_forces[i]->createNewCopy();
}

void TwoDScene::checkConsistency()
{
    assert( m_x.size() == m_v.size() );
    assert( m_x.size() == m_m.size() );
    assert( m_x.size() == (int) (4*m_fixed.size()) );
    
    assert( (m_x.array()==m_x.array()).all() );
    assert( (m_v.array()==m_v.array()).all() );
    assert( (m_m.array()==m_m.array()).all() );
    
    for( std::vector<std::pair<int,int> >::size_type i = 0; i < m_edges.size(); ++i )
    {
        assert( m_edges[i].first >= 0 );
        assert( m_edges[i].first < getNumParticles() );
        assert( m_edges[i].second >= 0 );
        assert( m_edges[i].second < getNumParticles() );
    }
    
    // TODO: Add more checks
}

std::vector<std::string>& TwoDScene::getParticleTags()
{
    return m_particle_tags;
}

const std::vector<std::string>& TwoDScene::getParticleTags() const
{
    return m_particle_tags;
}

