#include "TwoDSceneXMLParser.h"

void TwoDSceneXMLParser::loadExecutableSimulation( const std::string& file_name, bool rendering_enabled, ParticleSimulation** execsim, renderingutils::Viewport& view, scalar& dt, scalar& max_time, scalar& steps_per_sec_cap, renderingutils::Color& bgcolor, std::string& description, std::string& scenetag )
{
    // Load the xml document
    std::vector<char> xmlchars;
    rapidxml::xml_document<> doc;
    loadXMLFile( file_name, xmlchars, doc );
    
    // Attempt to locate the root node
    rapidxml::xml_node<>* node = doc.first_node("scene");
    if( node == NULL )
    {
        std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse xml scene file. Failed to locate root <scene> node. Exiting." << std::endl;
        exit(1);
    }
    
    // Determine what simulation type this is (particle, rigid body, etc)
    std::string simtype;
    loadSimulationType( node, simtype );
    
    // Parse common state
    loadMaxTime( node, max_time );
    loadMaxSimFrequency( node, steps_per_sec_cap );
    loadViewport( node, view );
    loadBackgroundColor( node, bgcolor );
    loadSceneDescriptionString( node, description );
    loadSceneTag( node, scenetag );
    
    // Parse the user-requested simulation type. The default is a particle simulation.
    if( simtype == "" || simtype == "particle-system" )
    {
        loadParticleSimulation(rendering_enabled, execsim, view, dt, bgcolor, node);
    }
    else if( simtype == "rigid-body" )
    {
        
        //std::cout << "MAX_TIME: " << max_time << std::endl;
        //std::cout << "DT: " << dt << std::endl;
    }
    else
    {
        std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Invalid simtype '" << simtype << "' specified. Valid options are 'particle-system' and 'rigid-body'. Exiting." << std::endl;
        exit(1);
    }
}

void TwoDSceneXMLParser::loadParticleSimulation(bool rendering_enabled, ParticleSimulation** execsim, renderingutils::Viewport& view, scalar& dt, renderingutils::Color& bgcolor, rapidxml::xml_node<>* node )
{
    TwoDScene* scene = new TwoDScene;
    
    // Scene
    loadParticles( node, *scene );
    loadEdges( node, *scene );
	
    // Integrator/solver
    SceneStepper* scene_stepper = NULL;
    loadIntegrator( node, &scene_stepper, dt );
    assert( scene_stepper != NULL );
    assert( dt > 0.0 );
    
    CollisionHandler* collision_handler = NULL;
	  
    
    TwoDSceneRenderer* scene_renderer = NULL;
    if( rendering_enabled )
    {
        scene_renderer = new TwoDSceneRenderer(*scene);
        scene_renderer->updateParticleSimulationState(*scene);
    }
    
    *execsim = new ParticleSimulation(scene, scene_stepper, scene_renderer);
}



void TwoDSceneXMLParser::loadXMLFile( const std::string& filename, std::vector<char>& xmlchars, rapidxml::xml_document<>& doc )
{
    // Attempt to read the text from the user-specified xml file
    std::string filecontents;
    if( !loadTextFileIntoString(filename,filecontents) )
    {
        std::cerr << "\033[31;1mERROR IN TWODSCENEXMLPARSER:\033[m XML scene file " << filename << ". Failed to read file." << std::endl;
        exit(1);
    }
    
    // Copy string into an array of characters for the xml parser
    for( int i = 0; i < (int) filecontents.size(); ++i ) xmlchars.push_back(filecontents[i]);
    xmlchars.push_back('\0');
    
    // Initialize the xml parser with the character vector
    doc.parse<0>(&xmlchars[0]);
}

bool TwoDSceneXMLParser::loadTextFileIntoString( const std::string& filename, std::string& filecontents )
{
    // Attempt to open the text file for reading
    std::ifstream textfile(filename.c_str(),std::ifstream::in);
    if(!textfile) return false;
    
    // Read the entire file into a single string
    std::string line;
    while(getline(textfile,line)) filecontents.append(line);
    
    textfile.close();
    
    return true;
}

void TwoDSceneXMLParser::loadSimulationType( rapidxml::xml_node<>* node, std::string& simtype )
{
    assert( node != NULL );
    rapidxml::xml_node<>* nd = node->first_node("simtype");
    
    if( node->first_node("simtype") ) if( node->first_node("simtype")->first_attribute("type") ) simtype = node->first_node("simtype")->first_attribute("type")->value();
}


/*void TwoDSceneXMLParser::loadHairs( rapidxml::xml_node<>* node, TwoDScene& scene){
    assert( node != NULL );
    
    // Count the number of particles, edges, and strands
    int numstrands = 0;
    int numparticles = 0;
    int numedges = 0;
    for( rapidxml::xml_node<>* nd = node->first_node("Hairs"); nd; nd = nd->next_sibling("Hairs") ){
        ++numstrands;
        int edgeCount = 0;
        for( rapidxml::xml_node<>* subnd = nd->first_node("particle"); subnd; subnd = subnd->next_sibling("particle") ){
            ++numparticles;
            ++edgeCount;
        }
        numedges += edgeCount - 1;
    }
    
    
    scene.resizeSystem( numparticles, numedges, numstrands );
    
    // compute vertex ID to DoF mapping
    std::vector<int> vert_to_dof( numparticles );
    Eigen::VectorXi dofVars( numparticles * 4 - numstrands + 1 );
    Eigen::VectorXi dofVerts( numparticles * 4 - numstrands + 1 );
    Eigen::Vector4i dofs = Vector4i( 0, 1, 2, 3 );
    
    std::vector<bool> tipVerts( numparticles );
    int dof = 0;
    numparticles = 0;
    for( rapidxml::xml_node<>* nd = node->first_node("StrandMaterialForces"); nd; nd = nd->next_sibling("StrandMaterialForces") ){
        for( rapidxml::xml_node<>* subnd = nd->first_node("particle"); subnd; subnd = subnd->next_sibling("particle") ){
            dofVars.segment<4>( dof ) = dofs;
            dofVerts.segment<4>( dof ).setConstant( numparticles );
            vert_to_dof[numparticles++] = dof;
            dof+= 4;
        }
        --dof;
        tipVerts[numparticles - 1] = true;
    }
    for( rapidxml::xml_node<>* nd = node->first_node("ParameterizedStrand"); nd; nd = nd->next_sibling("ParameterizedStrand") ){
        int nv = 0;
        if( nd->first_attribute("nv") )
        {
            std::string attribute(nd->first_attribute("nv")->value());
            if( !stringutils::extractFromString(attribute,nv) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse nv for strand " << numstrands << ". Value must be scalar. Exiting." << std::endl;
                exit(1);
            }
        }
        if(nv < 2) continue;
        for( int i = 0; i < nv; ++i)
        {
            dofVars.segment<4>( dof ) = dofs;
            dofVerts.segment<4>( dof ).setConstant( numparticles );
            vert_to_dof[numparticles++] = dof;
            dof+= 4;
        }
        --dof;
        tipVerts[numparticles - 1] = true;
    }
    dofVars.conservativeResize( numparticles * 4 - numstrands );
    dofVerts.conservativeResize( numparticles * 4 - numstrands );
    scene.setVertToDoFMap( vert_to_dof, dofVars, tipVerts, dofVerts );
    
    std::vector<std::string>& tags = scene.getParticleTags();
    int globalStrandID = 0;
    std::vector< std::vector< int > > particle_indices_vec;
    std::vector< std::vector<scalar> >particle_eta_vec;
    std::vector< std::vector< unsigned char > > particle_state_vec;
    std::vector< LIQUID_SIM_TYPE > liquidtype_vec;
    
    std::vector<Vector3s> particle_pos;
    Vector3s pos;
    Vector3s vel;
    int vtx = 0;
    int edx = 0;
    for( rapidxml::xml_node<>* nd = node->first_node("StrandMaterialForces"); nd; nd = nd->next_sibling("StrandMaterialForces") )
    {
        int paramsIndex = -1;
        if( nd->first_attribute("params") )
        {
            std::string attribute( nd->first_attribute("params")->value() );
            if( !stringutils::extractFromString( attribute, paramsIndex ) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of params (StrandParameters index) for StrandForce " << globalStrandID << ". Value must be integer. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of params (StrandParameters index) for StrandForce " << globalStrandID << ". Exiting." << std::endl;
            exit(1);
        }
        
        LIQUID_SIM_TYPE lst = LST_SHALLOW;
        rapidxml::xml_attribute<> *typend = nd->first_attribute("type");
        if( typend != NULL )
        {
            std::string handlertype(typend->value());
            if(handlertype == "shallow") lst = LST_SHALLOW;
            else
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Invalid flow 'type' attribute specified. Exiting." << std::endl;
                exit(1);
            }
        }
        liquidtype_vec.push_back( lst );
        
        std::vector<scalar> particle_eta;
        std::vector<unsigned char> particle_state;
        std::vector<int> particle_indices;
        particle_indices.clear();
        particle_pos.clear();
        int globalvtx = vtx;
        for( rapidxml::xml_node<>* subnd = nd->first_node("particle"); subnd; subnd = subnd->next_sibling("particle"))
        {
            particle_indices.push_back(vtx);
            
            scalar eta = 0.0;
            if( subnd->first_attribute("eta") )
            {
                std::string attribute(subnd->first_attribute("eta")->value());
                if( !stringutils::extractFromString(attribute,eta) )
                {
                    std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of eta attribute for film flow. Value must be numeric. Exiting." << std::endl;
                    exit(1);
                }
            }
            particle_eta.push_back(eta);
            
            bool isSource = false;
            if( subnd->first_attribute("source"))
            {
                std::string attribute(subnd->first_attribute("source")->value());
                if( !stringutils::extractFromString(attribute,isSource) )
                {
                    std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of source attribute for film flow. Value must be boolean. Exiting." << std::endl;
                    exit(1);
                }
            }
            particle_state.push_back(isSource);
            
            // Extract the particle's initial position
            pos.setZero();
            if( subnd->first_attribute("x") )
            {
                std::string position( subnd->first_attribute("x")->value() );
                if( !stringutils::readList( position, ' ', pos ) )
                {
                    std::cerr << "Failed to load x, y, and z positions for particle " << vtx << std::endl;
                    exit(1);
                }
            }
            else{
                std::cerr << "Failed to find x, y, and z position attributes for particle " << vtx << std::endl;
                exit(1);
            }
            scene.setPosition( vtx, pos );
            particle_pos.push_back(pos);
            
            // Extract the particle's initial velocity
            vel.setZero();
            if( subnd->first_attribute("v") )
            {
                std::string velocity( subnd->first_attribute("v")->value() );
                if( !stringutils::readList( velocity, ' ', vel ) )
                {
                    std::cerr << "Failed to load x, y, and z positions for particle " << vtx << std::endl;
                    exit(1);
                }
            }
            scene.setVelocity( vtx, vel );
            
            // Determine if the particle is fixed
            bool fixed = false;
            if( subnd->first_attribute("fixed") )
            {
                std::string attribute(subnd->first_attribute("fixed")->value());
                if( !stringutils::extractFromString(attribute,fixed) )
                {
                    std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of fixed attribute for particle " << vtx << ". Value must be boolean. Exiting." << std::endl;
                    exit(1);
                }
            }
            scene.setFixed( vtx, fixed );
            
            // Extract the particle's tag, if present
            if( subnd->first_attribute("tag") )
            {
                std::string tag(subnd->first_attribute("tag")->value());
                tags[vtx] = tag;
            }
            
            int group = 0;
            if( subnd->first_attribute("group") )
            {
                std::string attribute(subnd->first_attribute("group")->value());
                if( !stringutils::extractFromString(attribute,group) )
                {
                    std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of group attribute for particle " << vtx << ". Value must be integer. Exiting." << std::endl;
                    exit(1);
                }
            }
            scene.setScriptedGroup( vtx, group );
            
            if( vtx != globalvtx ){
                std::pair<int,int> newedge( vtx - 1, vtx );
                scene.setEdge( edx, newedge );
                scene.setEdgeRestLength( edx, ( scene.getPosition( newedge.first ) - scene.getPosition( newedge.second ) ).norm());
                ++edx;
            }
            
            ++vtx;
        }
        
        particle_indices_vec.push_back( particle_indices );
        particle_eta_vec.push_back( particle_eta );
        particle_state_vec.push_back( particle_state );
        
        scene.insertForce( new StrandForce( &scene, particle_indices, paramsIndex, globalStrandID++ ) );
        scene.insertStrandEquilibriumParameters( new StrandEquilibriumParameters( particle_pos, 0., 0., 0., 0., false ) );
    }
    
    for( rapidxml::xml_node<>* nd = node->first_node("ParameterizedStrand"); nd; nd = nd->next_sibling("ParameterizedStrand") )
    {
        int paramsIndex = -1;
        if( nd->first_attribute("params") )
        {
            std::string attribute( nd->first_attribute("params")->value() );
            if( !stringutils::extractFromString( attribute, paramsIndex ) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of params (StrandParameters index) for ParameterizedStrand " << globalStrandID << ". Value must be integer. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of params (StrandParameters index) for ParameterizedStrand " << globalStrandID << ". Exiting." << std::endl;
            exit(1);
        }
        
        LIQUID_SIM_TYPE lst = LST_SHALLOW;
        rapidxml::xml_attribute<> *typend = nd->first_attribute("type");
        if( typend != NULL )
        {
            std::string handlertype(typend->value());
            if(handlertype == "shallow") lst = LST_SHALLOW;
            else
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Invalid flow 'type' attribute specified. Exiting." << std::endl;
                exit(1);
            }
        }
        liquidtype_vec.push_back( lst );
        
        Vec3 initnorm;
        Vec3 startpoint;
        scalar length;
        scalar curl_radius = 0.0;
        scalar curl_density = 1e+63;
        int nv;
        
        rapidxml::xml_attribute<> *attr = nd->first_attribute("nx");
        if( attr != NULL )
        {
            std::string attribute( attr->value() );
            if( !stringutils::extractFromString( attribute, initnorm(0) ) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of nx for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
                exit(1);
            }
        } else {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of nx for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
            exit(1);
        }
        
        attr = nd->first_attribute("ny");
        if( attr != NULL )
        {
            std::string attribute( attr->value() );
            if( !stringutils::extractFromString( attribute, initnorm(1) ) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of ny for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
                exit(1);
            }
        } else {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of ny for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
            exit(1);
        }
        
        attr = nd->first_attribute("nz");
        if( attr != NULL )
        {
            std::string attribute( attr->value() );
            if( !stringutils::extractFromString( attribute, initnorm(2) ) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of nz for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
                exit(1);
            }
        } else {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of nz for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
            exit(1);
        }
        
        attr = nd->first_attribute("px");
        if( attr != NULL )
        {
            std::string attribute( attr->value() );
            if( !stringutils::extractFromString( attribute, startpoint(0) ) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of px for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
                exit(1);
            }
        } else {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of px for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
            exit(1);
        }
        
        attr = nd->first_attribute("py");
        if( attr != NULL )
        {
            std::string attribute( attr->value() );
            if( !stringutils::extractFromString( attribute, startpoint(1) ) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of py for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
                exit(1);
            }
        } else {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of py for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
            exit(1);
        }
        
        attr = nd->first_attribute("pz");
        if( attr != NULL )
        {
            std::string attribute( attr->value() );
            if( !stringutils::extractFromString( attribute, startpoint(2) ) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of pz for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
                exit(1);
            }
        } else {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of pz for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
            exit(1);
        }
        
        attr = nd->first_attribute("length");
        if( attr != NULL )
        {
            std::string attribute( attr->value() );
            if( !stringutils::extractFromString( attribute, length ) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of length for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
                exit(1);
            }
        } else {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of length for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
            exit(1);
        }
        
        attr = nd->first_attribute("nv");
        if( attr != NULL )
        {
            std::string attribute( attr->value() );
            if( !stringutils::extractFromString( attribute, nv ) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of nv for ParameterizedStrand " << globalStrandID << ". Value must be integer. Exiting." << std::endl;
                exit(1);
            }
        } else {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of nv for ParameterizedStrand " << globalStrandID << ". Value must be integer. Exiting." << std::endl;
            exit(1);
        }
        
        attr = nd->first_attribute("curlradius");
        if( attr != NULL )
        {
            std::string attribute( attr->value() );
            if( !stringutils::extractFromString( attribute, curl_radius ) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of curlradius for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
                exit(1);
            }
        } else {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of curlradius for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
            exit(1);
        }
        
        attr = nd->first_attribute("curldensity");
        if( attr != NULL )
        {
            std::string attribute( attr->value() );
            if( !stringutils::extractFromString( attribute, curl_density ) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of curldensity for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
                exit(1);
            }
        } else {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of curldensity for ParameterizedStrand " << globalStrandID << ". Value must be real. Exiting." << std::endl;
            exit(1);
        }
        
        scalar dL = length / (double) (nv - 1);
        std::vector<Vec3> strand_vertices;
        
        genCurlyHair(initnorm, startpoint, dL, nv, strand_vertices, curl_radius, curl_density, dL);
        
        int global_vtx = vtx;
        for(int i = 0; i < nv; ++i)
        {
            scene.setPosition(global_vtx + i, strand_vertices[i]);
        }
        
        std::vector<scalar> particle_eta;
        std::vector<unsigned char> particle_state;
        std::vector<int> particle_indices;
        particle_indices.clear();
        particle_pos.clear();
        for( rapidxml::xml_node<>* subnd = nd->first_node("particle"); subnd; subnd = subnd->next_sibling("particle"))
        {
            particle_indices.push_back(vtx);
            
            scalar eta = 0.0;
            if( subnd->first_attribute("eta") )
            {
                std::string attribute(subnd->first_attribute("eta")->value());
                if( !stringutils::extractFromString(attribute,eta) )
                {
                    std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of eta attribute for film flow. Value must be numeric. Exiting." << std::endl;
                    exit(1);
                }
            }
            particle_eta.push_back(eta);
            
            bool isSource = false;
            if( subnd->first_attribute("source"))
            {
                std::string attribute(subnd->first_attribute("source")->value());
                if( !stringutils::extractFromString(attribute,isSource) )
                {
                    std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of source attribute for film flow. Value must be boolean. Exiting." << std::endl;
                    exit(1);
                }
            }
            particle_state.push_back(isSource);
            
            // Extract the particle's initial velocity
            vel.setZero();
            if( subnd->first_attribute("v") )
            {
                std::string velocity( subnd->first_attribute("v")->value() );
                if( !stringutils::readList( velocity, ' ', vel ) )
                {
                    std::cerr << "Failed to load x, y, and z positions for particle " << vtx << std::endl;
                    exit(1);
                }
            }
            scene.setVelocity( vtx, vel );
            
            // Determine if the particle is fixed
            bool fixed = false;
            if( subnd->first_attribute("fixed") )
            {
                std::string attribute(subnd->first_attribute("fixed")->value());
                if( !stringutils::extractFromString(attribute,fixed) )
                {
                    std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of fixed attribute for particle " << vtx << ". Value must be boolean. Exiting." << std::endl;
                    exit(1);
                }
            }
            scene.setFixed( vtx, fixed );
            
            // Extract the particle's tag, if present
            if( subnd->first_attribute("tag") )
            {
                std::string tag(subnd->first_attribute("tag")->value());
                tags[vtx] = tag;
            }
            
            int group = 0;
            if( subnd->first_attribute("group") )
            {
                std::string attribute(subnd->first_attribute("group")->value());
                if( !stringutils::extractFromString(attribute,group) )
                {
                    std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of group attribute for particle " << vtx << ". Value must be integer. Exiting." << std::endl;
                    exit(1);
                }
            }
            scene.setScriptedGroup( vtx, group );
            
            if( vtx != global_vtx ){
                std::pair<int,int> newedge( vtx - 1, vtx );
                scene.setEdge( edx, newedge );
                scene.setEdgeRestLength( edx, ( scene.getPosition( newedge.first ) - scene.getPosition( newedge.second ) ).norm());
                ++edx;
            }
            
            ++vtx;
        }
        
        while(vtx < global_vtx + nv)
        {
            particle_indices.push_back(vtx);
            particle_eta.push_back(0.0);
            particle_state.push_back(false);
            scene.setVelocity(vtx, Vec3::Zero());
            scene.setFixed(vtx, false);
            scene.setScriptedGroup(vtx, 0);
            if(vtx != global_vtx) {
                std::pair<int,int> newedge( vtx - 1, vtx );
                scene.setEdge( edx, newedge );
                scene.setEdgeRestLength( edx, ( scene.getPosition( newedge.first ) - scene.getPosition( newedge.second ) ).norm());
                ++edx;
            }
            ++vtx;
        }
        
        particle_indices_vec.push_back( particle_indices );
        particle_eta_vec.push_back( particle_eta );
        particle_state_vec.push_back( particle_state );
        
        scene.insertForce( new StrandForce( &scene, particle_indices, paramsIndex, globalStrandID++ ) );
        scene.insertStrandEquilibriumParameters( new StrandEquilibriumParameters( strand_vertices, curl_radius, curl_density, dL, dL, true ) );
    }
    
    scene.computeMassesAndRadiiFromStrands();
    
    scene.getFilmFlows().resize(particle_indices_vec.size(), NULL);
    
    threadutils::thread_pool::ParallelFor(0, (int) particle_indices_vec.size(), [&] (int p) {
        scene.getFilmFlows()[p] = new CylindricalShallowFlow<3>( &scene,
                                                                particle_indices_vec[p],
                                                                Eigen::Map<VectorXs>(&particle_eta_vec[p][0], particle_eta_vec[p].size()),
                                                                particle_state_vec[p] );
    });
}*/




void TwoDSceneXMLParser::loadParticles( rapidxml::xml_node<>* node, TwoDScene& twodscene )
{
    assert( node != NULL );
    
    // Count the number of particles
    int old_num_particles = twodscene.getNumParticles();
    int numparticles = 0;
    for( rapidxml::xml_node<>* nd = node->first_node("particle"); nd; nd = nd->next_sibling("particle") ) ++numparticles;
    
    twodscene.resizeSystem(numparticles);
    
    //std::cout << "Num particles " << numparticles << std::endl;
    
    std::vector<std::string>& tags = twodscene.getParticleTags();
    
    int particle = 0;
    for( rapidxml::xml_node<>* nd = node->first_node("particle"); nd; nd = nd->next_sibling("particle") )
    {
        // Extract the particle's initial position
		Vector3s pos = Vector3s::Zero();
        if( nd->first_attribute("px") )
        {
            std::string attribute(nd->first_attribute("px")->value());
            if( !stringutils::extractFromString(attribute,pos.x()) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of px attribute for particle " << particle << ". Value must be numeric. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse px attribute for particle " << particle << ". Exiting." << std::endl;
            exit(1);
        }
        
        if( nd->first_attribute("py") )
        {
            std::string attribute(nd->first_attribute("py")->value());
            if( !stringutils::extractFromString(attribute,pos.y()) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of py attribute for particle " << particle << ". Value must be numeric. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse py attribute for particle " << particle << ". Exiting." << std::endl;
            exit(1);
        }
        twodscene.setPosition( particle, pos );
        
        // Extract the particle's initial velocity
		Vector3s vel = Vector3s::Zero();
        if( nd->first_attribute("vx") )
        {
            std::string attribute(nd->first_attribute("vx")->value());
            if( !stringutils::extractFromString(attribute,vel.x()) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of vx attribute for particle " << particle << ". Value must be numeric. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse vx attribute for particle " << particle << ". Exiting." << std::endl;
            exit(1);
        }
        
        if( nd->first_attribute("vy") )
        {
            std::string attribute(nd->first_attribute("vy")->value());
            if( !stringutils::extractFromString(attribute,vel.y()) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of vy attribute for particle " << particle << ". Value must be numeric. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse vy attribute for particle " << particle << ". Exiting." << std::endl;
            exit(1);
        }
        twodscene.setVelocity( particle, vel );
        
        // Extract the particle's mass
        scalar mass = std::numeric_limits<scalar>::signaling_NaN();
        if( nd->first_attribute("m") )
        {
            std::string attribute(nd->first_attribute("m")->value());
            if( !stringutils::extractFromString(attribute,mass) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of m attribute for particle " << particle << ". Value must be numeric. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse m attribute for particle " << particle << ". Exiting." << std::endl;
            exit(1);
        }
        twodscene.setMass( particle, mass );
        
        // Determine if the particle is fixed
        bool fixed;
        if( nd->first_attribute("fixed") )
        {
            std::string attribute(nd->first_attribute("fixed")->value());
            if( !stringutils::extractFromString(attribute,fixed) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of fixed attribute for particle " << particle << ". Value must be boolean. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse fixed attribute for particle " << particle << ". Exiting." << std::endl;
            exit(1);
        }
        twodscene.setFixed( particle, fixed );
        
        // Extract the particle's radius, if present
        scalar radius = 0.1;
        if( nd->first_attribute("radius") )
        {
            std::string attribute(nd->first_attribute("radius")->value());
            if( !stringutils::extractFromString(attribute,radius) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse radius attribute for particle " << particle << ". Value must be scalar. Exiting." << std::endl;
                exit(1);
            }
        }
		twodscene.setRadius( particle, radius );
        
        // Extract the particle's tag, if present
        if( nd->first_attribute("tag") )
        {
            std::string tag(nd->first_attribute("tag")->value());
            tags[particle] = tag;
        }
        
        //std::cout << "Particle: " << particle << "    x: " << pos.transpose() << "   v: " << vel.transpose() << "   m: " << mass << "   fixed: " << fixed << std::endl;
        //std::cout << tags[particle] << std::endl;
        
        ++particle;
    }
}

void TwoDSceneXMLParser::loadSceneTag( rapidxml::xml_node<>* node, std::string& scenetag )
{
    assert( node != NULL );
    
    if( node->first_node("scenetag") )
    {
        if( node->first_node("scenetag")->first_attribute("tag") )
        {
            scenetag = node->first_node("scenetag")->first_attribute("tag")->value();
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of tag attribute for scenetag. Value must be string. Exiting." << std::endl;
            exit(1);
        }
    }
}


void TwoDSceneXMLParser::loadEdges( rapidxml::xml_node<>* node, TwoDScene& twodscene )
{
    assert( node != NULL );
    
    twodscene.clearEdges();
    
    int edge = 0;
    for( rapidxml::xml_node<>* nd = node->first_node("edge"); nd; nd = nd->next_sibling("edge") )
    {
        std::pair<int,int> newedge;
        if( nd->first_attribute("i") )
        {
            std::string attribute(nd->first_attribute("i")->value());
            if( !stringutils::extractFromString(attribute,newedge.first) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of i attribute for edge " << edge << ". Value must be integer. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of i attribute for edge " << edge << ". Exiting." << std::endl;
            exit(1);
        }
        
        if( nd->first_attribute("j") )
        {
            std::string attribute(nd->first_attribute("j")->value());
            if( !stringutils::extractFromString(attribute,newedge.second) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of j attribute for edge " << edge << ". Value must be integer. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of j attribute for edge " << edge << ". Exiting." << std::endl;
            exit(1);
        }
        
        scalar radius = 0.015;
        if( nd->first_attribute("radius") )
        {
            std::string attribute(nd->first_attribute("radius")->value());
            if( !stringutils::extractFromString(attribute,radius) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse radius attribute for edge " << edge << ". Value must be scalar. Exiting." << std::endl;
                exit(1);
            }
        }
        
        //std::cout << "Edge: " << edge << "    i: " << newedge.first << "   j: " << newedge.second << std::endl;
        
        twodscene.insertEdge( newedge, radius );
        
        ++edge;
    }
}


void TwoDSceneXMLParser::loadIntegrator( rapidxml::xml_node<>* node, SceneStepper** scenestepper, scalar& dt )
{
    assert( node != NULL );
    
    dt = -1.0;
    
    // Attempt to locate the integrator node
    rapidxml::xml_node<>* nd = node->first_node("integrator");
    if( nd == NULL )
    {
        std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m No integrator specified. Exiting." << std::endl;
        exit(1);
    }
    
    // Attempt to load the integrator type
    rapidxml::xml_attribute<>* typend = nd->first_attribute("type");
    if( typend == NULL )
    {
        std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m No integrator 'type' attribute specified. Exiting." << std::endl;
        exit(1);
    }
    std::string integratortype(typend->value());
    
    if( integratortype == "explicit-euler" ) *scenestepper = new ExplicitEuler;
    else if( integratortype == "forward-backward-euler" || integratortype == "symplectic-euler" ) *scenestepper = new SemiImplicitEuler;
    else if( integratortype == "implicit-euler" ) *scenestepper = new ImplicitEuler;
    else if( integratortype == "linearized-implicit-euler" ) *scenestepper = new LinearizedImplicitEuler;
    else
    {
        std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Invalid integrator 'type' attribute specified. Exiting." << std::endl;
        exit(1);
    }
    
    // Attempt to load the timestep
    rapidxml::xml_attribute<>* dtnd = nd->first_attribute("dt");
    if( dtnd == NULL )
    {
        std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m No integrator 'dt' attribute specified. Exiting." << std::endl;
        exit(1);
    }
    
    dt = std::numeric_limits<scalar>::signaling_NaN();
    if( !stringutils::extractFromString(std::string(dtnd->value()),dt) )
    {
        std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse 'dt' attribute for integrator. Value must be numeric. Exiting." << std::endl;
        exit(1);
    }
    
    //std::cout << "Integrator: " << (*scenestepper)->getName() << "   dt: " << dt << std::endl;
}

void TwoDSceneXMLParser::loadMaxTime( rapidxml::xml_node<>* node, scalar& max_t )
{
    assert( node != NULL );
    
    // Attempt to locate the duraiton node
    rapidxml::xml_node<>* nd = node->first_node("duration");
    if( nd == NULL )
    {
        std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m No duration specified. Exiting." << std::endl;
        exit(1);
    }
    
    // Attempt to load the duration value
    rapidxml::xml_attribute<>* timend = nd->first_attribute("time");
    if( timend == NULL )
    {
        std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m No duration 'time' attribute specified. Exiting." << std::endl;
        exit(1);
    }
    
    max_t = std::numeric_limits<scalar>::signaling_NaN();
    if( !stringutils::extractFromString(std::string(timend->value()),max_t) )
    {
        std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse 'time' attribute for duration. Value must be numeric. Exiting." << std::endl;
        exit(1);
    }
}

void TwoDSceneXMLParser::loadViewport(rapidxml::xml_node<>* node, renderingutils::Viewport &view)
{
    assert( node != NULL );
    
    if(node->first_node("viewport") )
    {
        rapidxml::xml_attribute<> *cx = node->first_node("viewport")->first_attribute("cx");
        if(cx == NULL)
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m No viewport 'cx' attribute specified. Exiting." << std::endl;
            exit(1);
        }
        if(!stringutils::extractFromString(std::string(cx->value()),view.cx))
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse 'cx' attribute for viewport. Value must be scalar. Exiting." << std::endl;
            exit(1);
        }
        rapidxml::xml_attribute<> *cy = node->first_node("viewport")->first_attribute("cy");
        if(cy == NULL)
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m No viewport 'cy' attribute specified. Exiting." << std::endl;
            exit(1);
        }
        if(!stringutils::extractFromString(std::string(cy->value()),view.cy))
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse 'cy' attribute for viewport. Value must be scalar. Exiting." << std::endl;
            exit(1);
        }
        rapidxml::xml_attribute<> *size = node->first_node("viewport")->first_attribute("size");
        if(size == NULL)
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m No viewport 'size' attribute specified. Exiting." << std::endl;
            exit(1);
        }
        if(!stringutils::extractFromString(std::string(size->value()),view.size))
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse 'size' attribute for viewport. Value must be scalar. Exiting." << std::endl;
            exit(1);
        }
    }
}

void TwoDSceneXMLParser::loadMaxSimFrequency( rapidxml::xml_node<>* node, scalar& max_freq )
{
    assert( node != NULL );
    
    // Attempt to locate the duraiton node
    if( node->first_node("maxsimfreq") )
    {
        // Attempt to load the duration value
        rapidxml::xml_attribute<>* atrbnde = node->first_node("maxsimfreq")->first_attribute("max");
        if( atrbnde == NULL )
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m No maxsimfreq 'max' attribute specified. Exiting." << std::endl;
            exit(1);
        }
        
        if( !stringutils::extractFromString(std::string(atrbnde->value()),max_freq) )
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse 'max' attribute for maxsimfreq. Value must be scalar. Exiting." << std::endl;
            exit(1);
        }
    }
}




void TwoDSceneXMLParser::loadBackgroundColor( rapidxml::xml_node<>* node, renderingutils::Color& color )
{
    if( rapidxml::xml_node<>* nd = node->first_node("backgroundcolor") )
    {
        // Read in the red color channel
        double red = -1.0;
        if( nd->first_attribute("r") )
        {
            std::string attribute(nd->first_attribute("r")->value());
            if( !stringutils::extractFromString(attribute,red) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of r attribute for backgroundcolor. Value must be scalar. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of r attribute for backgroundcolor. Exiting." << std::endl;
            exit(1);
        }
        
        if( red < 0.0 || red > 1.0 )
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of r attribute for backgroundcolor. Invalid color specified. Valid range is " << 0.0 << "..." << 1.0 << std::endl;
            exit(1);
        }
        
        
        // Read in the green color channel
        double green = -1.0;
        if( nd->first_attribute("g") )
        {
            std::string attribute(nd->first_attribute("g")->value());
            if( !stringutils::extractFromString(attribute,green) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of g attribute for backgroundcolor. Value must be scalar. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of g attribute for backgroundcolor. Exiting." << std::endl;
            exit(1);
        }
        
        if( green < 0.0 || green > 1.0 )
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of g attribute for backgroundcolor. Invalid color specified. Valid range is " << 0.0 << "..." << 1.0 << std::endl;
            exit(1);
        }
        
        
        // Read in the blue color channel
        double blue = -1.0;
        if( nd->first_attribute("b") )
        {
            std::string attribute(nd->first_attribute("b")->value());
            if( !stringutils::extractFromString(attribute,blue) )
            {
                std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of b attribute for backgroundcolor. Value must be scalar. Exiting." << std::endl;
                exit(1);
            }
        }
        else
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of b attribute for backgroundcolor. Exiting." << std::endl;
            exit(1);
        }
        
        if( blue < 0.0 || blue > 1.0 )
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m Failed to parse value of b attribute for backgroundcolor. Invalid color specified. Valid range is " << 0.0 << "..." << 1.0 << std::endl;
            exit(1);
        }
        
        //std::cout << red << "   " << green << "   " << blue << std::endl;
        
        color.r = red;
        color.g = green;
        color.b = blue;
    }
}

void TwoDSceneXMLParser::loadSceneDescriptionString( rapidxml::xml_node<>* node, std::string& description_string )
{
    assert( node != NULL );
    
    description_string = "No description specified.";
    
    // Attempt to locate the integrator node
    rapidxml::xml_node<>* nd = node->first_node("description");
    if( nd != NULL ) 
    {
        rapidxml::xml_attribute<>* typend = nd->first_attribute("text"); 
        if( typend == NULL ) 
        {
            std::cerr << "\033[31;1mERROR IN XMLSCENEPARSER:\033[m No text attribute specified for description. Exiting." << std::endl;
            exit(1);
        }
        description_string = typend->value();
    }
}



