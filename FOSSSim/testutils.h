//
//  testutils.hpp
//  FOSSSim
//
//  Created by chang xiao on 9/16/17.
//
//

#ifndef testutils_hpp
#define testutils_hpp

#include <Eigen/StdVector>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <time.h>

#include <tclap/CmdLine.h>

// TODO: Clean these up, we don't need them all anymore.
#include "TwoDScene.h"
#include "Force.h"
#include "ExplicitEuler.h"
#include "SemiImplicitEuler.h"
#include "TwoDimensionalDisplayController.h"
#include "TwoDSceneRenderer.h"
#include "TwoDSceneXMLParser.h"
#include "TwoDSceneSerializer.h"
#include "StringUtilities.h"
#include "MathDefs.h"
#include "TimingUtilities.h"
#include "RenderingUtilities.h"
#include "YImage.h"
#include "CollisionHandler.h"

#include "ParticleSimulation.h"

void testEigenConcat();

#endif /* testutils_hpp */
