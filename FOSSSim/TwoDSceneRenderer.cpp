#include "TwoDSceneRenderer.h"
#include "TwoDimensionalDisplayController.h"

const Vector3s particle_color = Vector3s(0.650980392156863,0.294117647058824,0.0);
const Vector3s edge_color = Vector3s(0.0,0.388235294117647,0.388235294117647);

TwoDSceneRenderer::TwoDSceneRenderer( const TwoDScene& scene)
:
m_circle_points()
, m_semi_circle_points()
{
  initializeCircleRenderer(16);
  initializeSemiCircleRenderer(16);
}

void TwoDSceneRenderer::initializeCircleRenderer( int num_points )
{
  m_circle_points.resize(num_points);
  double dtheta = 2.0*PI/((double)num_points);
  for( int i = 0; i < num_points; ++i )
  {
    m_circle_points[i].first =  cos(((double)i)*dtheta);
    m_circle_points[i].second = sin(((double)i)*dtheta);
  }
}

void TwoDSceneRenderer::initializeSemiCircleRenderer( int num_points )
{
  double dtheta = PI/((double)(num_points-1));
  m_semi_circle_points.resize(num_points);
  for( int i = 0; i < num_points; ++i )
  {
    m_semi_circle_points[i].first =  -sin(((double)i)*dtheta);
    m_semi_circle_points[i].second = cos(((double)i)*dtheta);
  }  
}


void TwoDSceneRenderer::updateParticleSimulationState( const TwoDScene& scene )
{
}


void TwoDSceneRenderer::renderSolidCircle( const Eigen::Vector2d& center, double radius ) const
{  
	glBegin(GL_TRIANGLE_FAN);
    glVertex2d(center.x(),center.y());
    
    for( std::vector<std::pair<double,double> >::size_type i = 0; i < m_circle_points.size(); ++i )
      glVertex2d(radius*m_circle_points[i].first+center.x(), radius*m_circle_points[i].second+center.y());

    glVertex2d(radius*m_circle_points.front().first+center.x(), radius*m_circle_points.front().second+center.y());  
	glEnd();  
}

void TwoDSceneRenderer::renderCircle( const Eigen::Vector2d& center, double radius ) const
{
  glBegin(GL_LINE_LOOP);
    for( std::vector<Eigen::Vector2d>::size_type i = 0; i < m_circle_points.size(); ++i )
      glVertex2d(radius*m_circle_points[i].first+center.x(), radius*m_circle_points[i].second+center.y());
  glEnd();
}

void TwoDSceneRenderer::renderImpulse( const TwoDScene &scene, const CollisionInfo &impulse, bool buggy) const
{
  assert(impulse.m_idx1 < scene.getNumParticles());
  buggy ? glColor3d(1,0,0) : glColor3d(0,1,0);

  glBegin(GL_LINES);
  double x = scene.getX()[2*impulse.m_idx1];
  double y = scene.getX()[2*impulse.m_idx1+1];
  glVertex2d(x,y);
  glVertex2d(x + impulse.m_n[0], y+impulse.m_n[1]);
  glEnd();
}

void TwoDSceneRenderer::renderSweptEdge( const Eigen::Vector2d& x0, const Eigen::Vector2d& x1, double radius ) const
{
  Eigen::Vector2d e = x1-x0;
  double length = e.norm();
  double theta = 360.0*atan2(e.y(),e.x())/(2.0*PI);

  glPushMatrix();
  
  glTranslated(x0.x(), x0.y(),0.0);
  glRotated(theta, 0.0, 0.0, 1.0);
  
  glBegin(GL_TRIANGLE_FAN);
  glVertex2d(0.0,0.0);
  for( std::vector<Eigen::Vector2d>::size_type i = 0; i < m_semi_circle_points.size(); ++i )
    glVertex2d(radius*m_semi_circle_points[i].first, radius*m_semi_circle_points[i].second);
  for( std::vector<Eigen::Vector2d>::size_type i = 0; i < m_semi_circle_points.size(); ++i )
    glVertex2d(-radius*m_semi_circle_points[i].first+length, -radius*m_semi_circle_points[i].second);
  glVertex2d(radius*m_semi_circle_points.front().first, radius*m_semi_circle_points.front().second);
	glEnd();  
  
  glPopMatrix();
}

void TwoDSceneRenderer::renderHalfplane( const VectorXs &x, const VectorXs &n) const
{
}

void TwoDSceneRenderer::renderParticleSimulation( const TwoDScene& scene ) const
{
  const VectorXs& x = scene.getX();
  assert( x.size()%4 == 0 );
  assert( 4*scene.getNumParticles() == x.size() );
  
  // Render edges
  const std::vector<std::pair<int,int> >& edges = scene.getEdges();
  glColor3dv(edge_color.data());
  for( std::vector<std::pair<int,int> >::size_type i = 0; i < edges.size(); ++i )
  {
    assert( edges[i].first >= 0 );  assert( edges[i].first < scene.getNumParticles() );
    assert( edges[i].second >= 0 ); assert( edges[i].second < scene.getNumParticles() );

    renderSweptEdge( x.segment<2>(4*edges[i].first), x.segment<2>(4*edges[i].second), (scene.getRadius()[edges[i].first] + scene.getRadius()[edges[i].second]) * 0.5 );
  }
	
  // Render particles
//  const std::vector<scalar>& radii = scene.getRadii();
	glColor3dv(particle_color.data());
  for( int i = 0; i < scene.getNumParticles(); ++i )
  {
    renderSolidCircle( x.segment<2>(4*i), scene.getRadius()[i] );
  }  
}

void TwoDSceneRenderer::circleMajorParticleSimulationResiduals( const TwoDScene& oracle_scene, const TwoDScene& testing_scene, const std::vector<CollisionInfo> *impulses, const std::vector<CollisionInfo> *otherimpulses, scalar eps ) const
{
}
