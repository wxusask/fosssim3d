//
//  ParticleSimulation.cpp
//  FOSSSim
//
//  Created by chang xiao on 9/15/17.
//
//

#include "ParticleSimulation.h"
ParticleSimulation::ParticleSimulation( TwoDScene* scene, SceneStepper* scene_stepper, TwoDSceneRenderer* scene_renderer)
: m_scene(scene)
, m_scene_stepper(scene_stepper)
, m_scene_renderer(scene_renderer)
, m_display_controller(1920, 1080)
{
    
}

ParticleSimulation::~ParticleSimulation(){
    if( m_scene != NULL )
    {
        delete m_scene;
        m_scene = NULL;
    }
    
    if( m_scene_stepper != NULL )
    {
        delete m_scene_stepper;
        m_scene_stepper = NULL;
    }
    
    if( m_scene_renderer != NULL )
    {
        delete m_scene_renderer;
        m_scene_renderer = NULL;
    }
}

void ParticleSimulation::stepSystem(const scalar &dt){
    assert( m_scene != NULL );
    assert( m_scene_stepper != NULL );
    
    VectorXs oldpos = m_scene->getX();
    VectorXs oldvel = m_scene->getV();
    
    // Step the simulated scene forward
    m_scene_stepper->stepScene( *m_scene, dt );
    
    // Check for obvious problems in the simulated scene
#ifdef DEBUG
    m_scene->checkConsistency();
#endif
}

void ParticleSimulation::initializeOpenGLRenderer(){
    
}


void ParticleSimulation::renderSceneOpenGL(){
    assert( m_scene_renderer != NULL );
    m_scene_renderer->renderParticleSimulation(*m_scene);
    
}

void ParticleSimulation::updateOpenGLRendererState(){
    assert( m_scene_renderer != NULL );
    m_scene_renderer->updateParticleSimulationState(*m_scene);
}

void ParticleSimulation::computeCameraCenter(renderingutils::Viewport &view){
    const VectorXs& x = m_scene->getX();
    
    // Compute the bounds on all particle positions
    scalar max_x = -std::numeric_limits<scalar>::infinity();
    scalar min_x =  std::numeric_limits<scalar>::infinity();
    scalar max_y = -std::numeric_limits<scalar>::infinity();
    scalar min_y =  std::numeric_limits<scalar>::infinity();
	scalar max_z = -std::numeric_limits<scalar>::infinity();
	scalar min_z =  std::numeric_limits<scalar>::infinity();
    for( int i = 0; i < m_scene->getNumParticles(); ++i )
    {
        if( x(4*i) > max_x )   max_x = x(4*i);
        if( x(4*i) < min_x )   min_x = x(4*i);
        if( x(4*i+1) > max_y ) max_y = x(4*i+1);
        if( x(4*i+1) < min_y ) min_y = x(4*i+1);
		if( x(4*i+2) > max_z ) max_z = x(4*i+2);
		if( x(4*i+2) < min_z ) min_z = x(4*i+2);
    }
	
    // Set center of view to center of bounding box
    view.cx = 0.5*(max_x+min_x);
    view.cy = 0.5*(max_y+min_y);
	view.cz = 0.5*(max_z+min_z);
    
    // Set the zoom such that all particles are in view
    view.rx = 0.5*(max_x-min_x);
    if( view.rx == 0.0 ) view.rx = 1.0;
    view.ry = 0.5*(max_y-min_y);
    if( view.ry == 0.0 ) view.ry = 1.0;
	view.rz = 0.5*(max_z-min_z);
	if( view.rz == 0.0 ) view.rz = 1.0;
}



void ParticleSimulation::serializeScene(std::ofstream &outputstream){
    assert( m_scene != NULL );
    m_scene_serializer.serializeScene( *m_scene, outputstream );
    
}

void ParticleSimulation::centerCamera(bool b_reshape)
{
	renderingutils::Viewport view;
	
	computeCameraCenter(view);
	scalar ratio;
	
	ratio = ((scalar)m_display_controller.getWindowHeight())/((scalar)m_display_controller.getWindowWidth());
	
	view.size = 1.2*std::max(ratio*view.rx,view.ry);
	
	m_display_controller.setCenterX(view.cx);
	m_display_controller.setCenterY(view.cy);
	m_display_controller.setCenterZ(view.cz);
	m_display_controller.setScaleFactor(view.size);
	m_display_controller.initCamera(view);
	
	if(b_reshape) m_display_controller.reshape(m_display_controller.getWindowWidth(),m_display_controller.getWindowHeight());
}

std::string ParticleSimulation::getSolverName(){
    assert( m_scene_stepper != NULL );
    return m_scene_stepper->getName();
}

void ParticleSimulation::keyboard( unsigned char key, int x, int y )
{
	m_display_controller.keyboard(key,x,y);
}


void ParticleSimulation::reshape( int w, int h )
{
	m_display_controller.reshape(w,h);
}


void ParticleSimulation::special( int key, int x, int y )
{
	m_display_controller.special(key, x, y);
}


void ParticleSimulation::mouse( int button, int state, int x, int y )
{
	m_display_controller.mouse(button,state,x,y);
}

void ParticleSimulation::motion( int x, int y )
{
	m_display_controller.motion(x,y);
}


int ParticleSimulation::getWindowWidth() const
{
	return m_display_controller.getWindowWidth();
}


int ParticleSimulation::getWindowHeight() const
{
	return m_display_controller.getWindowHeight();
}


void ParticleSimulation::setWindowWidth(int w)
{
	m_display_controller.setWindowWidth(w);
}

void ParticleSimulation::setWindowHeight(int h)
{
	m_display_controller.setWindowHeight(h);
}

TwoDimensionalDisplayController* ParticleSimulation::getDC()
{
	return &m_display_controller;
}

void ParticleSimulation::setCenterX( double x )
{
	m_display_controller.setCenterX(x);
}

void ParticleSimulation::setCenterY( double y )
{
	m_display_controller.setCenterY(y);
}

void ParticleSimulation::setCenterZ( double z )
{
	m_display_controller.setCenterZ(z);
}

void ParticleSimulation::setScaleFactor( double scale )
{
	m_display_controller.setScaleFactor(scale);
}

void ParticleSimulation::setCamera( const Camera& cam )
{
	m_display_controller.getCamera() = cam;
	
	m_display_controller.setCenterX(cam.center_(0));
	m_display_controller.setCenterY(cam.center_(1));
	m_display_controller.setCenterZ(cam.center_(2));
	
	scalar size = 1.2 * cam.radius_;
	m_display_controller.setScaleFactor(size);
}

void ParticleSimulation::setView( const renderingutils::Viewport& view )
{
	m_display_controller.setCenterX(view.cx);
	m_display_controller.setCenterY(view.cy);
	m_display_controller.setCenterZ(view.cz);
	m_display_controller.setScaleFactor(view.size);
	m_display_controller.initCamera(view);

}














